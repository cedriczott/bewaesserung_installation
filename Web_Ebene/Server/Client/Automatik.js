//-----------------------------------------------------------------------------
//---Automatik - Modus: -------------------------------------------------------
//---Seiten Laden und erneuern-------------------------------------------------
//---Softwareprojekt ET19A Gruppe 13-------------------------------------------
//---Version 1.2---------------------------------------------------------------
//-----------------------------------------------------------------------------

//Serveranfrage, Antwort und Funktionsaufrauf der Timespaces
document.addEventListener('DOMContentLoaded', function() {
    fetch('/api/python/message')
        .then(response => response.json())
        .then(data => Load_Table_Timespace(data.data));
});

//Sorgt daf�r das die Infos erneuert werden
var idVar = setInterval(() => {
    fetch('/api/python/message')
        .then(response => response.json())
        .then(data => Load_Table_Timespace(data.data));
}, 500);

//------------------------------------------------------------------------------
//---Um aenderungen zu schicken ------------------------------------------------
//------------------------------------------------------------------------------

//Zum setzen vom Automatikmodus
//Parameter: keine
//R�ckgabewert: keinen
function Automodus() {

    //Erstelle Objekt welches mit der Serveranfrage verschickt wird
    let data = {
        operation_mode: 'Automatik-Modus'
    };

    //Optionen der Serveranfrage
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    };

    //Serveranfrage
    let result = fetch('/api/post_settings', options)
        .then(response => response.json());
    console.log(result);
}

//Um neuen Timespace hinzuf�gen zu k�nnen
//Parameter: Start- und Endzeit vom Bewaesserungsintervals 
//           in Stunden und Minuten (ID wird sp�ter hinzugef�gt)
//R�ckgabewert: keinen
function New_Timespace(start_h, start_min, end_h, end_min, areas) {

    let temporary_id = 0;
    //Erstelle Objekt welches mit der Serveranfrage verschickt wird
    let data = {
        timespace_time: [
            temporary_id,
            start_min,
            start_h,
            end_min,
            end_h,
            areas
        ]
    };

    //Optionen der Serveranfrage
    let options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    };

    //Serveranfrage und Antwort
    let result = fetch('/api/new/timespace', options)
        .then(response => response.json());
    console.log(result);
}

//Bearbeiten der Zeit eingabe damit diese wie gewuenscht �bergeben wird
//Parameter: keine
//R�ckgabewert: keine
function Convert_Timespace() {

    //Selektieren der Felder 
    let startTime = document.getElementById('set_start_time');
    let endTime = document.getElementById('set_end_time');
    let check_box_1 = document.getElementById('check1');
    let check_box_2 = document.getElementById('check2');
    let check_box_3 = document.getElementById('check3');

    console.log(startTime.value);
    //Pr�fe ob beide Uhrzeiten eingetragen sind
    if (startTime.value == 0  || endTime.value == 0) {
        alert('Lege Start- und Endzeit fest');
        return;
    }

    //Pr�fe ob mindestens ein Bereich ausgewaehlt ist
    if (check_box_1.checked === false && check_box_2.checked === false) {
        if (check_box_3.checked === false) {
            alert(unescape('W%E4hle mindestens einen Bereich aus'));
            return;
        }
    }

    //Zerstueckelung der Strings in Stunde und Minute
    let start_hours = startTime.value.slice(0, 2);
    let start_min = startTime.value.substring(3, 5);
    let end_hours = endTime.value.slice(0, 2);
    let end_min = endTime.value.substring(3, 5);

    let areas = [check_box_1.checked, check_box_2.checked, check_box_3.checked];

    //Funktionsaufrug um die Eingabe weiter zu leiten f�r die Server anfrage
    New_Timespace(start_hours, start_min, end_hours, end_min, areas);
}

//Serveranfrage zum loeschen eines Zeitintervals
//Parameter: ID der Zeit die geloescht werden soll
//R�ckgabewert: keine       
function Delete_Timespace(id) {

    //Erstelle Objekt welches mit der Serveranfrage verschickt wird
    let data = {
        timespace_delete: id,
    };

    //Optionen der Serveranfrage
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    };

    //Serveranfrage und Antwort
    let result = fetch('/api/post_settings', options)
        .then(response => response.json());
    console.log(result);
}

//------------------------------------------------------------------------------
//---Seitenverhalten------------------------------------------------------------
//------------------------------------------------------------------------------

//Erzeugt die Tabelle mit den Timespaces
//Parameter: Datenbankresponse 
//R�ckgabewert: keine
function Load_Table_Timespace(data) {

    //An welcher Stelle die Werte reingeschrieben werden
    let table = document.querySelector('#table_content');

    //Falls keine Zeitraume gespeichert sind
    if (data.timespace.length === 0) {
        table.innerHTML = `<tr><td class='no-data' colspan='3'>` +
            'Keine Zeiten gespeichert!</td></tr>';
        return;
    }

    let tableHtml = '';
    //Geht die laenge im Arry durch un schreibt sie in die Tabelle 
    for (var i = 0; i < data.timespace.length; i++) {
        tableHtml += '<tr>';
        //Anzeige Zeitraum
        tableHtml += `<td>${data.timespace[i][1]}</td>`;

        //Fuer welche Bereiche es zaehlt
        tableHtml += `<td><table><tbody class='inner-table'><tr>`;
        //Bereich 1
        if (data.timespace[i][2][0] === true) {
            tableHtml += `<td>1</td>`;
        }
        //Bereich 2
        if (data.timespace[i][2][1] === true) {
            tableHtml += `<td>2</td>`;
        }
        //Bereich 3
        if (data.timespace[i][2][2] === true) {
            tableHtml += `<td>3</td>`;
        }
        tableHtml += '</tr></tbody></table></td>';

        //Der Button zum loeschen 
        tableHtml += `<td><button`+
        ` onclick='Delete_Timespace(${data.timespace[i][0]})'`+
        ` class='btn btn-primary' style='background-color: #009933;`+
        ` border: #009933' data-id='${data.timespace[i][0]}'>L&oumlschen</td>`;
        tableHtml += '</tr>';
    }

    table.innerHTML = tableHtml;
}