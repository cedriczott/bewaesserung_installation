//-----------------------------------------------------------------------------
//---Winter - Modus:-----------------------------------------------------------
//---Softwareprojekt ET19A Gruppe 13-------------------------------------------
//---Version 1.2---------------------------------------------------------------
//-----------------------------------------------------------------------------

//Zum setzen vom Wintermodus
//Parameter: keine
//R�ckgabewert: keinen
function wintermodus() {

    //Erstelle Objekt welches mit der Serveranfrage verschickt wird
    let data = {
        'operation_mode': 'Wintermodus',
        'manual_state': 'false'
    };

    //Optionen der Serveranfrage
    let options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    };

    //Serveranfrage und Antwort
    let result = fetch('/api/post_settings', options)
        .then(response => response.json());
    console.log(result);
}
