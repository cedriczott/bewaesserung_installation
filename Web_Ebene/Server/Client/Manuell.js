﻿//-----------------------------------------------------------------------------
//---Manuell - Modus: ---------------------------------------------------------
//---Seite Laden und erneuern--------------------------------------------------
//---Softwareprojekt ET19A Gruppe 13-------------------------------------------
//---Version 1.2---------------------------------------------------------------
//-----------------------------------------------------------------------------

//Serveranfrage, Antwort und Funktionsaufrauf f�r das Seiten Verahlten
document.addEventListener('DOMContentLoaded', function () {
    fetch('/api/python/message')
        .then(response => response.json())
        .then(data => Webside_Behavior(data.data));
});

//Sorgt daf�r das die Infos erneuert werdenn
var idVar = setInterval(() => {
    fetch('/api/python/message')
        .then(response => response.json())
        .then(data => Webside_Behavior(data.data));
}, 500);
 
//------------------------------------------------------------------------------
//---Um aenderungen zu schicken ------------------------------------------------
//------------------------------------------------------------------------------

//Zum starten vom Manuellmodus
//Parameter: keine
//R�ckgabewert: keinen
function Manuellmodus() {

    //Erstelle Objekt welches mit der Serveranfrage verschickt wird
    let data = {
        operation_mode: 'Manuell-Modus'
    };

    //Optionen der Serveranfrage
    let options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    };

    //Serveranfrage
    let result = fetch('/api/post_settings', options)
        .then(response => response.json());
    console.log(result);
}

//Bereich 1: Zum an oder Ausschalten der Bewaesserung
//Parameter: An/Aus BOOL 
//R�ckgabewert: keinen
function Area_1 (watering) {

    //Erstelle Objekt welches mit der Serveranfrage verschickt wird
    let data = {
        area: 0,
        watering_state: watering
    };

    //Optionen der Serveranfrage
    let options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    };

    //Serveranfrage und Antwort
    let result = fetch('/api/manuell', options)
        .then(response => response.json());
    console.log(result);
}

//Bereich 2: Zum an oder Ausschalten der Bewaesserung
//Parameter: An/Aus BOOL 
//R�ckgabewert: keinen
function Area_2 (watering) {

    //Erstelle Objekt welches mit der Serveranfrage verschickt wird
    let data = {
        area: 1,
        watering_state: watering
    };

    //Optionen der Serveranfrage
    let options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    };

    //Serveranfrage und Antwort
    let result = fetch('/api/manuell', options)
        .then(response => response.json());
    console.log(result);
}

//Bereich 3: Zum an oder Ausschalten der Bewaesserung
//Parameter: An/Aus BOOL 
//R�ckgabewert: keinen
function Area_3 (watering) {

    //Erstelle Objekt welches mit der Serveranfrage verschickt wird
    let data = {
        area: 2,
        watering_state: watering
    };

    //Optionen der Serveranfrage
    let options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    };

    //Serveranfrage und Antwort
    let result = fetch('/api/manuell', options)
        .then(response => response.json());
    console.log(result);
}

//------------------------------------------------------------------------------
//---Seitenverhalten------------------------------------------------------------
//------------------------------------------------------------------------------

//Erzeugt die Tabelle 
//Parameter: Akktuele Zustaende JSON
//R�ckgabewert: keine
function Webside_Behavior(data) {

    //Knoepfe anzeigen lassen wenn der Manuellmodus aktiv ist
    if (data.operation_mode === 'Manuell-Modus') {
        $(document).ready(function () {  
            $('#manuell_state_btn1').show(500);
            $('#manuell_state_btn2').show(500);
            $('#manuell_state_btn3').show(500);
            $('.help_txt').show(500);
        });
    }
    else {
        $(document).ready(function () {
            $('#manuell_state_btn1').hide();
            $('#manuell_state_btn2').hide();
            $('#manuell_state_btn3').hide();
            $('.help_txt').hide();
        });
    }

    //Kommt teils noch als string
    console.log(data);

    //Bereich 1: Welcher Button als aktiv dargestellt werden soll
    if (data.manual_state[0] === true) {
        $('#watering_on_1').removeClass('btn btn-lg btn-default').addClass('btn btn-lg btn-primary active');
        $('#watering_off_1').removeClass('btn btn-lg btn-primary active').addClass('btn btn-lg btn-default');
    }
    else {
        $('#watering_off_1').removeClass('btn btn-lg btn-default').addClass('btn btn-lg btn-primary active');
        $('#watering_on_1').removeClass('btn btn-lg btn-primary active').addClass('btn btn-lg btn-default');
    }

    //Bereich 2: Welcher Button als aktiv dargestellt werden soll
    if (data.manual_state[1] === true) {
        $('#watering_on_2').removeClass('btn btn-lg btn-default').addClass('btn btn-lg btn-primary active');
        $('#watering_off_2').removeClass('btn btn-lg btn-primary active').addClass('btn btn-lg btn-default');
    }
    else {
        $('#watering_off_2').removeClass('btn btn-lg btn-default').addClass('btn btn-lg btn-primary active');
        $('#watering_on_2').removeClass('btn btn-lg btn-primary active').addClass('btn btn-lg btn-default');
    }

    //Bereich 3: Welcher Button als aktiv dargestellt werden soll
    if (data.manual_state[2] === true) {
        $('#watering_on_3').removeClass('btn btn-lg btn-default').addClass('btn btn-lg btn-primary active');
        $('#watering_off_3').removeClass('btn btn-lg btn-primary active').addClass('btn btn-lg btn-default');
    }
    else {
        $('#watering_off_3').removeClass('btn btn-lg btn-default').addClass('btn btn-lg btn-primary active');
        $('#watering_on_3').removeClass('btn btn-lg btn-primary active').addClass('btn btn-lg btn-default');
    }
}