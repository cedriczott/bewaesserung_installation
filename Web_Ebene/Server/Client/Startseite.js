//------------------------------------------------------------------------------
//---Startseite: ---------------------------------------------------------------
//---Inhalt erneuern------------------------------------------------------------
//---Softwareprojekt ET19A Gruppe 13--------------------------------------------
//---Version 1.2----------------------------------------------------------------
//------------------------------------------------------------------------------

//Serveranfrage, Antwort und Funktionsaufrauf zum Laden der Tabelle
document.addEventListener('DOMContentLoaded', function() {
    fetch('/api/python/message')
        .then(response => response.json())
        .then(data => Load_HTML_Startpage(data.data));
});


//Sorgt dafuer das die Infos erneuert werden
var idVar = setInterval(() => {
    fetch('/api/python/message')
        .then(response => response.json())
        .then(data => Load_HTML_Startpage(data.data));
}, 500);


//Erzeugt die Startpageinfos
//Parameter: Akktuele Zustaende JSON
//R�ckgabewert: keine
function Load_HTML_Startpage(data) {

    //Anzeige falles es keine Werte gibt
    if (data === undefined) {
        let content_startpage = document.querySelector('#operation_mode');
        content_startpage.innerHTML =
        `<h>Keine Informationen verf&uumlgbar</h>`;
        return;
    }
    else {
        //Anzeige fuer den Modus
        let mode = document.querySelector('#operation_mode');
        mode.innerHTML = `${data.operation_mode}`;
    }

    //Anzeige fuer die Temperatur
    let temp = document.querySelector('#temperature');
    temp.innerHTML = `${data.temperature}`;

    //Anzeige fuer die Frostwarnung
    if (data.frost_warning === true) {
        $(document).ready(function() {
            $('#frost_warning').show(1000);
        });
    } else {
        $(document).ready(function() {
            $('#frost_warning').hide(1000);
        });
    }

    //Anzeige ob die Bewaesserung an oder aus ist
    let watering = document.querySelector('#watering_area_state');

    let content = '';

    console.log(data.watering_area_state[0]);
    //Bereich 1
    if (data.watering_area_state[0] === true) {
        content += '<tr><th>Bereich 1:</th><th>AN</th></tr>';
    } else {
        content += '<tr><th>Bereich 1:</th><th>AUS</th></tr>';
    }

    //Bereich 2
    if (data.watering_area_state[1] === true) {
        content += '<tr><th>Bereich 2:</th><th>AN</th></tr>';
    } else {
        content += '<tr><th>Bereich 2:</th><th>AUS</th></tr>';
    }

    //Bereich 3
    if (data.watering_area_state[2] === true) {
        content += '<tr><th>Bereich 3:</th><th>AN</th></tr>';
    } else {
        content += '<tr><th>Bereich 3:</th><th>AUS</th></tr>';
    }

    watering.innerHTML = content;
}