//-----------------------------------------------------------------------------
//---Verhalten aller Seiten----------------------------------------------------
//---Softwareprojekt ET19A Gruppe 13-------------------------------------------
//---Version 1.2---------------------------------------------------------------
//-----------------------------------------------------------------------------


//Sorgt daf�r das die Uhrzeit jede Sekunde erneuert wird
var idVar = setInterval(() => {
    timer();
}, 1000);

//Die Uhrzeit ausgeben
//Parameter: (Class time)
//R�ckgabewert: Akktuelle Uhrzeit
function timer() {
    var dateVar = new Date();
    var time = dateVar.toLocaleTimeString();
    document.getElementById('clock').innerHTML = time;
}
