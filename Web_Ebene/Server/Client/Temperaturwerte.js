//-----------------------------------------------------------------------------
//---Temperaturwertetabelle: --------------------------------------------------
//---Seite Laden---------------------------------------------------------------
//---Softwareprojekt ET19A Gruppe 13-------------------------------------------
//---Version 1.2---------------------------------------------------------------
//-----------------------------------------------------------------------------

//Serveranfrage, Antwort und Funktionsaufrauf zum Laden der Tabelle (alle Werte)
document.addEventListener('DOMContentLoaded', function() {
    fetch('/getTemperature')
        .then(response => response.json())
        .then(data => Load_HTML_Table(data.data));
});

//------------------------------------------------------------------------------
//---Suchfunktion---------------------------------------------------------------
//------------------------------------------------------------------------------

//Um einen Bestimmten Datumsbereich sich anzeigen zu lassen
//Parameter: keine
//R�ckgabewert: keinen
function Search_Date() {

    //Selektieren der zwei eingabe Felder 
    let start_date = document.getElementById('first_date');
    let end_date = document.getElementById('second_date');

    //Pr�fe ob beide Eingaben vorhanden sind
    if (start_date.value == 0 || end_date.value == 0) {
        alert('Lege Anfangs- und Enddatum fest');
        return;
    }

    //Erstelle Objekt welches mit der Serveranfrage verschickt wird
    let data = {
        start_date: start_date.value,
        end_date: end_date.value
    };

    //Optionen der Serveranfrage
    let options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    };

    //Serveranfrage, Antwort und Funktion zum Laden der Tabelle
    let result = fetch('/api/search', options)
        .then(response => response.json())
        .then(data => Load_HTML_Table(data.data));
    console.log(result);
}

//------------------------------------------------------------------------------
//---Erstellt Tabell------------------------------------------------------------
//------------------------------------------------------------------------------

//Erzeugt die Tabelle 
//Parameter: data (Datenbankresponse)
//R�ckgabewert: keine
function Load_HTML_Table(data) {

    //An welcher Stelle die Werte reingeschrieben werden
    let table = document.querySelector('table tbody');

    //Anzeige falles es keine Werte gibt
    if (data.length === 0) {
        table.innerHTML = `<tr><td colspan='2'>F&uumlr diesem Zeitraum gibt` +
            ' es keine Temperaturwerte!<br>(Beachte! Nach 4 Wochen werden die' +
            ' Daten automatisch gel&oumlscht.)</td></tr> ';
        return;
    }

    let tableHtml = '';
    //Erstellt fuer jedes Wertepaar eine Tabellenzeile
    data.forEach(function({ Datum, Temperatur }) {
        tableHtml += '<tr>';
        tableHtml += `<td>${new Date(Datum).toLocaleString()}</td>`;
        tableHtml += `<td>${Temperatur}</td>`;
        tableHtml += '</tr>';
    });

    table.innerHTML = tableHtml;
}