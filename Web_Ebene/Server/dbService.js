﻿//------------------------------------------------------------------------------
//---Datenbank------------------------------------------------------------------
//---Softwareprojekt ET19A Gruppe 13-------------------------------------------
//---Version 1.2---------------------------------------------------------------
//------------------------------------------------------------------------------

//Notwendige Module
const mysql = require('mysql');
const dotenv = require('dotenv');
dotenv.config();

//Datenbankverbindung herstellen
const connection = mysql.createConnection({
    host: process.env.HOST,
    user: process.env.DB_USERNAME,
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
    port: process.env.DB_PORT
});

//Ueberpruefe Datenbankverbindung
connection.connect((err) => {
    if (err) {
        console.log(err.message);
    }
    console.log('db ' + connection.state);
});

//------------------------------------------------------------------------------
//---Datenbank-Klasse mit den Datenbankaufrufen---------------------------------
//------------------------------------------------------------------------------

let instance = null;

class DB_Service {

    //Konstruktor
    //Parameter: Keine
    //R�ckgabewert: Datenbankobjekt
    static Get_DB_Service_Instance() {
        return instance ? instance : new DB_Service();
    }

    //Abfrage der Tabelle aussentemperatur
    //Parameter: Keine
    //R�ckgabewert: Gesamte Tabelle
    async Get_All_Temperatures() {

        try {
            let response = await new Promise((resolve, reject) => {

                //Gibt die ganze Tabelle zurueck mit dem neusten Datum als erstes
                let query = 'SELECT * FROM aussentemperatur ORDER BY Datum DESC;';

                connection.query(query, (err, results) => {
                    if (err) {
                        reject(new Error(err.message));
                    }
                    resolve(results);
                });
            });
            return response;     
        }

        //Gibt eine Fehlermeldung aus falls nichts zur�ckgegeben werden kann
        catch (error) {
            console.log(error);
        }
    }

    //Suche nach Datum
    //Parameter: Datumseingabe
    //R�ckgabewert: Gefundene Werte
    async Search_By_Date(data) {

        let time_value = JSON.parse(data);
        
        try {
            let response = await new Promise((resolve, reject) => {

                //Gibt Werte zwischen dem Datum zurueck
                let query = `SELECT * FROM aussentemperatur WHERE date(Datum)`+
                    ` BETWEEN date('${time_value.start_date}')`+
                    ` and date('${time_value.end_date}')`+
                    ` ORDER BY Datum DESC;`;

                connection.query(query, (err, results) => {
                    if (err){
                        reject(new Error(err.message));
                    } 
                    resolve(results);
                });
            });
            return response;
        }

        //Gibt eine Fehlermeldung aus falls nichts zur�ckgegeben werden kann
        catch (error) {
            
            console.log(error);
        }
    }
}
module.exports = DB_Service;