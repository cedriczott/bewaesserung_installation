//-----------------------------------------------------------------------------
//---Server-Datei--------------------------------------------------------------
//---Softwareprojekt ET19A Gruppe 13-------------------------------------------
//---Version 1.2---------------------------------------------------------------
//-----------------------------------------------------------------------------

//Notwendigen Module
const express = require('express');
const app = express();

const Joi = require('joi');
const cors = require('cors');
const dotenv = require('dotenv');
dotenv.config();

//Server ueber diesem Port verfuegbar
app.listen(process.env.PORT, () =>
  console.log(`Listening on port ${process.env.PORT}`)
);

//Einbindungen eigener Module
const dbService = require('./dbService');
const brokerService = require('./brokerService');

//Zur Kommunikation zwischen Server und Client
app.use(cors());
app.use(express.json({ limit: '1mb' }));
app.use(express.urlencoded({ extended: false }));


//------------------------------------------------------------------------------
//---HTML-Seiten bekommen-------------------------------------------------------
//------------------------------------------------------------------------------

//Damit beim Seiten aufruf die JS- und CSS-Files gefunden werden
app.use(express.static('Client'));

//Seite: Start/Home
//Parameter: URL, Server Anfrage und Anwort
//R�ckgabewert: keinen
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/Client/Startseite.html');
});

//Seite: Automatik-Modus
//Parameter: URL, Server Anfrage und Anwort
//R�ckgabewert: keinen
app.get('/auto', (req, res) => {
  res.sendFile(__dirname + '/Client/Automatik.html');
});

//Seite: Manuell-Modus
//Parameter: URL, Server Anfrage und Anwort
//R�ckgabewert: keinen
app.get('/manuell', (req, res) => {
  res.sendFile(__dirname + '/Client/Manuell.html');
});

//Seite: Winter-Modus
//Parameter: URL, Server Anfrage und Anwort
//R�ckgabewert: keinen
app.get('/winter', (req, res) => {
  res.sendFile(__dirname + '/Client/Winter.html');
});

//Seite: Gespeicherte Temperaturwerte
//Parameter: URL, Server Anfrage und Anwort
//R�ckgabewert: keinen
app.get('/data', (req, res) => {
  res.sendFile(__dirname + '/Client/Temperaturwerte.html');
});

//------------------------------------------------------------------------------
//---Client Anfragen Datenbank--------------------------------------------------
//------------------------------------------------------------------------------

//Zum Laden aller Datenbankwerte
//Parameter: URL, Server Anfrage und Anwort
//R�ckgabewert: keinen
app.get('/getTemperature', (req, res) => {
  let db = dbService.Get_DB_Service_Instance();

  let result = db.Get_All_Temperatures();

  //Schicke Datenbankergebnisse zurueck
  result
    .then((data) => res.json({ data: data }))
    .catch((err) => console.log(err));
});

//Zum Laden der Datenbankwerte eines bestimment Zeitraums
//Parameter: URL, Server Anfrage und Anwort
//R�ckgabewert: keinen
app.post('/api/search', (req, res) => {
  let db = dbService.Get_DB_Service_Instance();

  //Lege Pruefungsschema fuer das Datum fest
    const temp_date_schema = Joi.object({
      start_date: Joi.date().required(),
      end_date: Joi.date().required()
  }).with('start_date', 'end_date');
  console.log(req.body);

  //Ueberpruefe
  const {error} = temp_date_schema.validate(req.body);

  //Ergebnis
  //Erfolgreich: Datenbank abfrage
  //Gescheitert: Sende Bad Request
  if (error){
    return res.status(400).send(error.details[0].message);
  }

  else{
    //Datenbank abfrage
    let result = db.Search_By_Date(JSON.stringify(req.body));

    // Schicke Datenbankergebnisse zurueck
    result
    .then((data) => res.json({ data: data }))
    .catch((err) => console.log(err));
  }     
});

//------------------------------------------------------------------------------
//---Client Anfragen Zustaende aedern / (POST)----------------------------------
//------------------------------------------------------------------------------

//Zum senden der übergeben Werte an das Pythonprogramm
//Parameter: URL, Server Anfrage und Anwort
//Rückgabewert: keinen
app.post('/api/post_settings', (req, res) => {
  let broker = brokerService.Get_Broker_ServiceInstance();

  let result = broker.Send_Message(JSON.stringify(req.body));

  result
    .then((data) => res.json({ data: data }))
    .catch((err) => console.log(err));
});

//Zum senden der übergeben Werte an das Pythonprogramm
//Parameter: URL, Server Anfrage und Anwort
//Rückgabewert: keinen
app.post('/api/manuell', (req, res) => {
  let broker = brokerService.Get_Broker_ServiceInstance();

  let result = broker.Manual_Area(JSON.stringify(req.body));

  result
    .then((data) => res.json({ data: data }))
    .catch((err) => console.log(err));
});

//Zum senden der neuen Timespaces an das Pythonprogramm
//Parameter: URL, Server Anfrage und Anwort
//R�ckgabewert: keinen
app.post('/api/new/timespace', (req, res) => {
  let broker = brokerService.Get_Broker_ServiceInstance();

  let result = broker.New_Timespace(JSON.stringify(req.body));

  result
    .then((data) => res.json({ data: data }))
    .catch((err) => console.log(err));
});

//------------------------------------------------------------------------------
//---Client Anfragen Neue Daten auf der Seite bekommen--------------------------
//------------------------------------------------------------------------------

//Gibt die gespeicherten Zustaende weiter
//Parameter: URL, Server Anfrage und Anwort
//R�ckgabewert: keinen
app.get('/api/python/message', (req, res) => {
  let broker = brokerService.Get_Broker_ServiceInstance();

  let result = broker.Get_Complete_JSON();

  result
    .then((data) => res.json({ data: data }))
    .catch((err) => console.log(err));
});
