//------------------------------------------------------------------------------
//---MQTT-Verbindung------------------------------------------------------------
//---Softwareprojekt ET19A Gruppe 13-------------------------------------------
//---Version 1.2---------------------------------------------------------------
//------------------------------------------------------------------------------

//Notwendigen Module
const mqtt = require('mqtt');
const nodemailer = require('nodemailer');

//Verwendeter MQTT Server
const client = mqtt.connect('mqtt://test.mosquitto.org');

//Topic das vom Broker verwendet werden soll
const topic_output = process.env.NODE_MESSAGE;
const topic_input = process.env.PYTHON_MESSAGE;

//Der Broker muss einmal Aboniert werden
//Parameter: Verbinden
//R�ckgabewert: keinen
client.on('connect', function() {
    client.subscribe(topic_input, function(err) {
        if (!err) {
            console.log('subscribe');
        }
    });
});

//Um nachrichten zu erhalten
//Parameter: Nachricht
//R�ckgabewert: keinen
client.on('message', function(topic, message) {

    console.log('New message received');

    //Die erhaltene Nachricht wird zum Speichern weitergeleitet
    New_Message(message.toString());
});

//Falls bei der Verbindung ein Fehler auftritt
//Parameter: Error
//R�ckgabewert: keinen
client.on('error', function(error) {

    //Falles es sich zu dem MQTT Broker nicht verbinden kann
    //MQTT keepalive default liegt bei 60 sec.
    console.log('Can not connect: ' + error);
    process.exit(1);
});

//------------------------------------------------------------------------------
//---Klasse um Nachrichten auf den Broker zu laden------------------------------
//------------------------------------------------------------------------------
const instance = null;

class Broker_Service {

    //Konstruktor
    //Parameter: Keine
    //R�ckgabewert: Brokerobjekt
    static Get_Broker_ServiceInstance() {
        return instance ? instance : new Broker_Service();
    }

    //Veroeffentlicht die Nachricht auf dem Broker
    //Parameter: JSON Inhalt
    //R�ckgabewert: keine
    async Send_Message(message) {
        client.publish(topic_output, message);
        console.log('publish:');
        console.log(message);
    }

    //Veroffentllicht den neuen Intervall und f�gt eine ID hinzu
    //Parameter: JSON Object
    //          { timespace_time: [ID, start_m, start_h, end_m, end_h] }
    //R�ckgabewert: keine
    async New_Timespace(data) {

        var new_timespace = JSON.parse(data);

        let pos; //geht die Position durch
        let new_ID; //Neue ID
        let state;

        //Geht die IDs der bisherigen Timespaces durch
        //bis eine frei ID gefunden wurde
        for (new_ID = 1; new_ID <= all_timespaces.timespace.length + 1; new_ID++) {
            state = 0;
            for (pos in all_timespaces.timespace) {
                if (new_ID === parseInt(all_timespaces.timespace[pos][0])) {
                    console.log(all_timespaces.timespace[pos]);
                    state = 1;
                    break;
                }
            }

            //Gebe dem neum Zeitraum die freie ID mit
            if (state === 0) {
                new_timespace.timespace_time[0] = new_ID;
                break;
            }
        }

        //Auf dem Broker veroeffentlichen
        client.publish(topic_output, JSON.stringify(new_timespace));
        console.log('public new timespace');
        console.log(new_timespace);
    }

    //Vom Manuellem Modus einen Bereich an- oder ausschalten
    //Parameter: JSON Object {area: number , watering_state: bool} 
    //R�ckgabewert: keine
    async Manual_Area(data) {

        data = JSON.parse(data);
        console.log(latest_manual_state);
        console.log(data);
        //Aendere nur das eine Element
        latest_manual_state.manual_state[data.area] = data.watering_state;

        //Auf dem Broker veroeffentlichen
        client.publish(topic_output, JSON.stringify(latest_manual_state));
        console.log('publish:');
        console.log(latest_manual_state.manual_state);
    }

    //Gebe die alle Daten zurueck
    //Parameter: keine
    //R�ckgabewert: alle Akktuellen Daten
    async Get_Complete_JSON() {
        return all_data;
    }
}
module.exports = Broker_Service;

//------------------------------------------------------------------------------
//---Bearbeitung der Empfangenen strings von Python-----------------------------
//------------------------------------------------------------------------------

//Variablen zum Speichern der Zustaende
var all_data = {};
var latest_operation_mode = { operation_mode: undefined };
var latest_manual_state = { manual_state: [undefined, undefined, undefined] };
var all_timespaces = { timespace: [
        [undefined, undefined]
    ] };
var latest_frost_warning_state = { frost_warning: undefined };
var latest_temperature = { temperature: undefined };
var latest_watering_area_state = { watering_area_state: undefined };

//Filtern und in Variablen speichern
//Parameter: Message von Python 
//R�ckgabewert: keinen
function New_Message(MQTT_message) {

    //Ganze Nachricht
    let data = JSON.parse(MQTT_message);
    all_data = data;

    //Als naechstes werden die Nachrichten gefiltert 
    //und auf die Variablen geschoben

    if (data.operation_mode !== undefined || data.operation_mode !== null) {
        let send = {
            operation_mode: data.operation_mode
        };
        latest_operation_mode = send;
    }

    if (data.manual_state !== undefined || data.manual_state !== null) {
        let send = {
            manual_state: data.manual_state
        };
        latest_manual_state = send;
    }

    if (data.timespace !== undefined || data.timespace !== null) {
        let send = {
            timespace: data.timespace
        };
        all_timespaces = send;
    }

    if (data.frost_warning !== undefined || data.frost_warning !== null) {
        let send = {
            frost_warning: data.frost_warning
        };

        //Falls true wird die Funktion zum E-Mail senden aufgerufen
        if (send.frost_warning === true) {
            Send_Mail_Frost_Warning();
        }
        latest_frost_warning_state = send;
    }

    if (data.temperature !== undefined || data.temperature !== null) {
        let send = {
            temperature: data.temperature
        };
        latest_temperature = send;
    }

    if (data.watering_area_state !== undefined || data.watering_area_state !== null) {
        let send = {
            watering_area_state: data.tempwatering_area_stateerature
        };
        latest_watering_area_state = send;
    }
}

//------------------------------------------------------------------------------
//---Zum E-Mail versenden-------------------------------------------------------
//------------------------------------------------------------------------------

var limit_hour = 0;
var limit_day = 0;

//Sendet eine Mail
//Parameter: keine
//R�ckgabewert: keinen
function Send_Mail_Frost_Warning() {

    //Akktuellesdatum
    var hours = (new Date()).getHours();
    var day = (new Date()).getDate();

    //Bindingung damit pro Stunde nur eine Mail verschickt werden kann
    if (limit_hour !== hours || limit_day !== day) {

        //Anmeldung E-Mail Service
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'frostschutzsender@gmail.com',
                pass: 'Bewaesserung2021'
            }
        });

        //Text und Ziel kann nach belieben geaendert werden
        var mailOptions = {
            from: 'frostschutzsender@gmail.com',
            to: 'frostschutzziel@gmail.com',
            subject: 'Frostschutzwarnung',
            html: '<p>Achtung! Es ist sehr kalt!</p>' +
                '<p>Die Temperatur ist unter 5 Grad Celsius</p>'
        };

        //Sendet die E-Mail
        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
            }
        });

        //Akktualisiere Variablen
        limit_hour = hours;
        limit_day = day;
    }
}