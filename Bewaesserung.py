#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# ---Main-Programm Bewässerung-------------------------------------------------
# ---Softwareprojekt ET19A Gruppe 13-------------------------------------------
# ---Version 1.2---------------------------------------------------------------
# -----------------------------------------------------------------------------

print("Start Booting")

from Funktionstest import funcional_test
from temperaturedatabase import temperaturedatabase
from Controller import controller
from Initiate import initiate

# Initialisierung Sense-Hat
from sense_hat import SenseHat
sense = SenseHat()
sense.clear()

import time
import socket
import os
# Bibliothek für env-Benutzerdatenspeicherung
from dotenv import load_dotenv
load_dotenv(dotenv_path=".//Web_Ebene//Server//.env")

# -----------------------------------------------------------------------------
# ---Initialisierung Benutzerparameter aus env-Datei---------------------------
# -----------------------------------------------------------------------------
env_host=os.getenv("HOST")
env_db_user=os.getenv("DB_USERNAME")
env_db_pass=os.getenv("PASSWORD")
env_database=os.getenv("DATABASE")
env_db_port=os.getenv("DB_PORT")
env_server_port=os.getenv("PORT")
env_node_message=os.getenv("NODE_MESSAGE")
env_python_message=os.getenv("PYTHON_MESSAGE")

# Bibliothek für Aufruf des node.js Programms.
import subprocess

# Bibliothek für MQTT Broker
import paho.mqtt.client as mqtt

# Controller-Objekt Initialisierung mit letztem Speicherstand
init_object = initiate()
import_last_values = init_object.import_method()
if not import_last_values[0]:
    controller_object = controller(ini_env_python_message = env_python_message)
else:
    controller_object = controller(import_last_values[0], import_last_values[1], 
                                   ini_env_python_message = env_python_message)

# -----------------------------------------------------------------------------
# ---Hauptprogramm Python------------------------------------------------------
# -----------------------------------------------------------------------------
def main():
    # -------------------------------------------------------------------------
    # ---Datenbank initialisieren und Verbindung zum Server herstellen.--------
    # -------------------------------------------------------------------------
    database = temperaturedatabase(env_host, 
        env_db_user, env_db_pass, env_database)

    try:
         #Start des JavaScripts als Hintergrundprozess.
         subprocess.Popen(["node",
                    ".//app.js"], cwd = ".//Web_Ebene//Server")
    except:
        print("Fehler beim Server Start")

    while(True):
        try:
            with socket.create_connection([env_host, env_server_port]
                                          ) as sub_object:
                if(sub_object):
                    break
        except:
            pass

    # -------------------------------------------------------------------------
    # ---Verbindung zum MQTT-Broker herstellen---------------------------------
    # -------------------------------------------------------------------------
    client = mqtt.Client()
    client.connect("test.mosquitto.org")
    client.on_connect = on_connect
    client.on_message = on_message
    client.loop_start()

    # Zuweisung zu Callback-Funktion
    sense.stick.direction_any = joystick_pushed

    # Testroutine
    test = funcional_test()
    #test.test_timespace()
    #test.test_initiation()
    while True:
        #test.test_controller(controller_object, client, database, sense)
        #test.test_database(database)

        # Zyklischer Aufruf der Controller-Central Unit.
        controller_object.central_unit(client, database, sense)

        #Ohne Timer beste react time
        #time.sleep(2) 
    pass

# -----------------------------------------------------------------------------
# ---Callback-Funktion für MQTT-connect----------------------------------------
# -----------------------------------------------------------------------------
def on_connect(client, userdata, flags, rc):

    print("Connected with result code " + str(rc))

    # Subscribing in on_connect() führt zu einem neuen Subscribing, wenn die
    # Verbindung erneut aufgebaut wird.
    client.subscribe(env_node_message)
# -----------------------------------------------------------------------------
# ---Callback-Funktion für MQTT-message----------------------------------------
# -----------------------------------------------------------------------------
def on_message(client, userdata, msg):
    controller_object.write_json(msg.payload.decode("utf-8"))

# -----------------------------------------------------------------------------
# ---Callback-Funktion für Joystick-Eingriff-----------------------------------
# -----------------------------------------------------------------------------
def joystick_pushed(event):
    if event.action == 'pressed':
        if event.direction == 'up':
            controller_object.Senseprint("Manuell-Modus")
        elif event.direction == 'down':
            controller_object.Senseprint("Automatik-Modus")
        elif event.direction == 'left':
            controller_object.Senseprint("Wintermodus")
    pass
main()