#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# ---Controller-Klasse Bewässerung---------------------------------------------
# ---Softwareprojekt ET19A Gruppe 13-------------------------------------------
# ---Version 1.2---------------------------------------------------------------
# -----------------------------------------------------------------------------

from Initiate import initiate
from Timespace import timespace

from datetime import datetime
#import random
# Initialisierung Sense-Hat
#from sense_hat import SenseHat, ACTION_PRESSED, ACTION_RELEASED
#sense = SenseHat()
#sense.clear()

import json

import paho.mqtt.client as mqtt

# -----------------------------------------------------------------------------
# ---Zentrale Steuereinheit des Hauptprogramms. Sie Empfängt Daten anderer-----
# ---Klassen, wertet diese aus und Steuert andere Klassen----------------------
# -----------------------------------------------------------------------------
class controller(object):
    
  # Konstruktor
  # Parameter: keine
  # Rückgabe: keine  
  def __init__(self, operation_mode = "Manuell-Modus", timespace_controle = [], ini_env_python_message=""):
   
        self.operation_mode = operation_mode
        self.manual_state = [False, False, False ]
        self.timespace_controle = timespace_controle
        self.temperature_input = 0
        self.json_timespace_start_min = 0
        self.json_timespace_start_hour = 0
        self.json_timespace_end_min = 0
        self.json_timespace_end_hour = 0
        self.json_timespace_id = 0
        self.json_timespace_working_area = [False, False, False ]
        self.json_input = ""
        self.json_output = ""
        self.frost_warning = False
        self.watering_area_state = [ False, False, False ]
        self.temp_storage = 0
        self.env_python_message = ini_env_python_message
        self.border_minute = 0
        self.joysick_received = False

  # Destruktor
  # Parameter: keine
  # Rückgabe: keine 
  def __del__(self):

        pass
  # Methode regelt die Verbindung mit dem Server, bzw. der Webebene über JSON.
  # Parameter: 
     # client_param: Objekt des MQTT-Brokers
  # Rückgabe: keine
  def server_JSON_transfer(self, client_param):

        json_store = self.json_output
        
        if self.operation_mode != "Manuell-Modus":
            self.manual_state = [0,0,0]

        dictionary_output = {
            "operation_mode": self.operation_mode,
            "manual_state": self.manual_state,
            "timespace": self.timespace_controle,
            "frost_warning": self.frost_warning,
            "temperature": self.temperature_input,
            "watering_area_state": self.watering_area_state
        }

        self.json_output = json.dumps(dictionary_output)
        if (json_store != self.json_output):                
            (rc, mid) = client_param.publish(self.env_python_message, 
                            self.json_output)           
            #print("JSON Python-Output: ")
            #print(self.json_output)
            #print("Bewässerte Bereiche: ")
            #print(self.watering_area_state)
        pass
  # Methode regelt den Input des Sense-Hat Moduls.
  # Parameter: keine
  # Rückgabe: keine
  def sense_hat_input(self, sense_object):
        # Temperatursensor SenseHat einlesen.
        self.temperature_input = (round(sense_object.get_temperature(),1))

  # Methode regelt Output des Sense-Hat Moduls.
  # Parameter: keine
  # Rückgabe: keine
  def sense_hat_output(self, sense_object, op_mode_joystick = None):
        # Ausgabe welcher Bereich aktiv ist.
        if self.operation_mode == "Wintermodus":
            self.watering_area_state = [ False, False, False ]

        elif (self.operation_mode == "Manuell-Modus"): 
           counter1 = 0
           while (counter1 < len(self.manual_state)):
             if(self.manual_state[counter1] == True):
                self.watering_area_state[counter1] = True
             else:
                self.watering_area_state[counter1] = False
             counter1 += 1

        elif self.operation_mode == "Automatik-Modus": 
            if not self.timespace_controle:
                self.watering_area_state = [ False, False, False ]
            
            #state_timespace = False
            self.watering_area_state = [ False, False, False ]
            counter2 = 0
            while (counter2 < len(self.timespace_controle)):
                automatic_mode_timespace = self.timespace_controle[counter2]
                time_operationmode = timespace(
                timespace_string = automatic_mode_timespace[1], 
                timespace_id = automatic_mode_timespace[0])

                # Abfrage, ob aktuelle Zeit in Timespace liegt
                if  time_operationmode.is_between(str(datetime.now(
                    ).strftime("%H")), str(datetime.now().strftime(
                        "%M"))):
                    # Loop durch Bereichstuple und Speicherung für JSON-Übergabe
                    counter3 = 0
                    while (counter3 < len(automatic_mode_timespace[2])):
                        space = automatic_mode_timespace[2]
                        if space[counter3] == True:
                            self.watering_area_state[counter3] = True
                        counter3 += 1
                counter2 += 1

        
            # Ausgabe an den Sense-Hat als String (/PF150/)
        if (self.border_minute != int(datetime.now().strftime("%M"))):
            sense_object.show_message(str(self.temperature_input))
            self.border_minute =  int(datetime.now().strftime("%M"))

        elif self.joysick_received:
            # Anzeige der Änderung des Betriebsmodus
            sense_object.clear()
            sense_object.show_message(self.operation_mode, scroll_speed = 0.06)
            self.joysick_received = False
        
        else:
            #Anzeige der Bewässerten Bereiche auf Sense-Hat (/PF120/)
            green = (0, 255, 0)
            red = (255, 0, 0)
            
            if self.watering_area_state[0] == True:
                 for x in range(8):
                     sense_object.set_pixel(0, x, green)
                     sense_object.set_pixel(1, x, green)
            else:
                 for x in range(8):
                     sense_object.set_pixel(0, x, red)
                     sense_object.set_pixel(1, x, red)

            if self.watering_area_state[1] == True:
                 for x in range(8):
                     sense_object.set_pixel(3, x, green)
                     sense_object.set_pixel(4, x, green)
            else:
                 for x in range(8):
                     sense_object.set_pixel(3, x, red)
                     sense_object.set_pixel(4, x, red) 

            if self.watering_area_state[2] == True:
                 for x in range(8):
                     sense_object.set_pixel(6, x, green)
                     sense_object.set_pixel(7, x, green)
            else:
                 for x in range(8):
                     sense_object.set_pixel(6, x, red)
                     sense_object.set_pixel(7, x, red)

  # Methode sorgt für eine aktuelle Initialisierungs-Datei.
  # Parameter: keine
  # Rückgabe: keine
  def ini_update(self):

        update = initiate(self.operation_mode, self.timespace_controle)
        update.export_method()
        pass
  # Methode, welche Eingängswerte auswertet und Ausgangssignale steuert.
  # Parameter: 
     # client_param: Objekt des MQTT-Brokers
     # database: Objekt der Datenbank
  # Rückgabe: keine
  def central_unit(self, client_param, database, sensehat_object):
    
        # ---------------------------------------------------------------------
        # ---Steuerung in bestimmten Temperaturbereichen.----------------------
        # ---(/PF170/ und /PF180/)---------------------------------------------
        # ---------------------------------------------------------------------
        if self.temperature_input > 15 and self.operation_mode == "Wintermodus":
            self.operation_mode = "Automatik-Modus"
        elif self.temperature_input < 5:
            self.frost_warning = True
        elif self.temperature_input >= 5:
            self.frost_warning = False
        
        # Steuerung der Ausgänge und Ausgangssignale.
        self.server_JSON_transfer(client_param)
        self.sense_hat_output(sensehat_object)

        # Eingänge von SenseHat abfragen. 
        # Weitere Eingänge durch Callbacks ausgelesen.
        self.sense_hat_input(sense_object = sensehat_object)
        self.database_controle(database)
        self.ini_update()

        pass

  # Schnittstelle zur Datenbank. (/PD200/)
  # Parameter: 
     # database: Objekt der Datenbank
  # Rückgabe: keine
  def database_controle(self, database):

        database.db_date_delete()
        database.db_insert(self.temperature_input)
        pass
  # Methode führt json_input Callback Inhalt zu.
  # Parameter: 
     # input: Callback-String
  # Rückgabe: keine
  def write_json(self, input):

        self.json_input = str(input)
        #print("Json Input: ")
        #print(self.json_input)

        # Umwandlung in Python-Dictionary mittels loads-Funktion
        if (self.json_input != ""):
            json_dictionary = json.loads(self.json_input)

            # -----------------------------------------------------------------
            # ---Auswertung des Dictionarys und Überführung in Python-Variablen
            # -----------------------------------------------------------------
            if "operation_mode" in json_dictionary:
                self.operation_mode = json_dictionary["operation_mode"]

            if "manual_state" in json_dictionary:
                self.manual_state = json_dictionary["manual_state"]
                
            # Übergabe einer Timespace-Id, welche gelöscht werden soll.
            if "timespace_delete" in json_dictionary:
                
                for time_tuple in self.timespace_controle:
                    print(self.timespace_controle.index(time_tuple))
                    if time_tuple[0] == json_dictionary["timespace_delete"]:
                        self.timespace_controle.pop(
                            self.timespace_controle.index(time_tuple))
                        continue
                pass

            # Tupel speichert alle Daten des gesendeten Timespace:
            # [ID, Startzeit(Minuten), Startzeit(Stunden), Endzeit(Minuten), 
            # Endzeit(Stunden), Bereiche]
            if "timespace_time" in json_dictionary:
                timespace_times_tuple = json_dictionary["timespace_time"]
                self.json_timespace_id = timespace_times_tuple[0]
                self.json_timespace_start_min = timespace_times_tuple[1]
                self.json_timespace_start_hour = timespace_times_tuple[2]
                self.json_timespace_end_min = timespace_times_tuple[3]
                self.json_timespace_end_hour = timespace_times_tuple[4]
                self.json_timespace_working_area = timespace_times_tuple[5]

                timespc = timespace(self.json_timespace_id,
                   self.json_timespace_start_min, self.json_timespace_start_hour,
                   self.json_timespace_end_min, self.json_timespace_end_hour, 
                   self.json_timespace_working_area)
                self.timespace_controle.append(timespc.time_response())
                del timespc

  def Senseprint(self, msg):
        self.operation_mode = msg
        self.joysick_received = True