#!/bin/bash

#Ein Installationsskript für eine einfache Installation
#der benötigten Module und Bibliotheken.

#Zeitzone aendern
timedatectl set-timezone Europe/Berlin
#dpkg-reconfigure -f noninteractive tzdata

#Installation sense-hat Bibliothek
apt-get install sense-hat

#Installation pip-Installer für python
apt install python3-pip

#Installation node.js und npm
apt-get install nodejs 
apt-get install npm
npm install -g n
n stable

#zu installierende Pakete python
python3 -m pip install paho-mqtt mysql-connector-python python-dotenv sense-hat datetime

#Zu installierende Pakete JavaScript
npm install express cors dotenv mqtt nodemailer mysql socket.io joi

#Initialisierung MySQL
apt install mariadb-server
mysql -uroot <<MYSQL_SCRIPT
CREATE DATABASE IF NOT EXISTS temperature;
CREATE USER IF NOT EXISTS 'pi'@'localhost' IDENTIFIED BY 'Bewaesserung';
use temperature;
GRANT USAGE ON *.* TO 'pi'@'localhost' IDENTIFIED BY 'Bewaesserung';
GRANT ALL PRIVILEGES ON temperature.* TO 'pi'@'localhost';
FLUSH PRIVILEGES;
CREATE TABLE IF NOT EXISTS aussentemperatur (Datum datetime, Temperatur double);
MYSQL_SCRIPT

echo "MySQL User created"
echo "Username: pi"
echo "Host: localhost"
