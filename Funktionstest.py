#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# ---Test-Klasse Bewässerung---------------------------------------------------
# ---Softwareprojekt ET19A Gruppe 13-------------------------------------------
# ---Version 1.1---------------------------------------------------------------
# -----------------------------------------------------------------------------

from Initiate import initiate
from Timespace import timespace
import temperaturedatabase
from Controller import controller

import random
import time

# Bibliothek für MQTT Broker
import paho.mqtt.client as mqtt

# -----------------------------------------------------------------------------
# ---Klasse für Modultests-----------------------------------------------------
# -----------------------------------------------------------------------------

class funcional_test(object):
    # Funktionstest Timespace-Klasse
    # Parameter: keine
    # Rückgabe: keine
    def test_timespace(self):
        
        # time_space = Timespace.timespace(3, 30, 10, 25, 13)
        time_space = timespace(timespace_string = "08:00 - 09:30",
                                        timespace_id = 1)
        print(time_space.time_response())

        if(time_space.is_between("09","03")):
           print("Zeit dazwischen")
        else:
            print("nicht dazwischen")

    # Funktionstest Initiation-Klasse
    # Parameter: keine
    # Rückgabe: keine
    def test_initiation(self):

        time1 = timespace(1, 30, 10, 25, 13, [True, False, True])
        time2 = timespace(2, 10, 13, 20, 14, [True, True, False])
        operation_mode = "Manuell-Modus"
        timespaces_ini = []
        timespaces_ini.append(time1.time_response())
        timespaces_ini.append(time2.time_response())
        init = initiate(operation_mode,timespaces_ini)

        if init == -1:
            return

        # Funktionstest Export-Methode
        # ---------------------------------------------------------------------
        #init.export_method()
        #x = open("Initiation.txt")
        #print(x.read())
        # ---------------------------------------------------------------------

        # Funktionstest Import-Methode
        # ---------------------------------------------------------------------
        #print(init.import_method())
        # ---------------------------------------------------------------------
        del init

    # Funktionstest für die Controller-Klasse
    # Parameter: 
        # controller_object: Objekt der Controller-Klasse
        # client_param: Objekt des MQTT-Brokers
        # databse: Objekt der Datenbank
    # Rückgabe: keine
    def test_controller(self, controller_object, client_param, database, sense):

        # Central-Unit 
        #controller_object.central_unit(client_param, database, sense)

        # Initiation-Methode
        #controller_object.ini_update()
        
        # JSON Kommunikation
        #controller_object.server_JSON_transfer(client_param)

        pass
    
    # Funktionstest für die Datenbank-Klasse
    # Parameter: 
        # databse: Objekt der Datenbank
    # Rückgabe: keine
    def test_database(self, database):

    # Methodenaufruf
        database.db_date_check()
        database.db_insert(random.randint(-5, 25))
        pass





