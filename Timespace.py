#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# ---Timespace-Klasse Bewässerung----------------------------------------------
# ---Softwareprojekt ET19A Gruppe 13-------------------------------------------
# ---Version 1.1---------------------------------------------------------------
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# ---Klasse als Container für Automatikmodus Zeiträume-------------------------
# -----------------------------------------------------------------------------
class timespace(object):
 
  number_of_objects = 0

  # Konstruktor
  # Parameter: 
      # timespace_id: ID des Timespace-Objekts
      # startini_min: Startzeit Minutenangabe
      # startini_hours: Startzeit Stundenangabe
      # endini_min: Endzeit Minutenangabe
      # endini_hours: Endzeit Stundenangabe
      # timespace_string: Übergabe des Zeitraums als string
  # Rückgabe: keine
  def __init__(self, timespace_id, startini_min = 00, startini_hours = 00, 
               endini_min = 00, endini_hours = 00, working_area = [0,0,0], 
               timespace_string = ""):
    
    self.start_mins = startini_min
    self.start_hours = startini_hours
    self.end_mins = endini_min
    self.end_hours = endini_hours
    self.timespace_id = timespace_id
    self.response_tuple = []
    self.timespace_string = timespace_string
    self.working_area = working_area

  # Gibt Zeitraum des Objekts zurück.
  # Parameter: keine
  # Rückgabe:
      # response_tuple: (Tuple) [Id, Zeitrahmen des Objekts, [Bereich(e)]]
  def time_response(self):

    self.start_time = str(self.start_hours) + ":" + str(self.start_mins)
    self.end_time = str(self.end_hours) + ":" + str(self.end_mins)
    self.timeformat = self.start_time + " - " + self.end_time
    self.response_tuple.append(self.timespace_id)
    self.response_tuple.append(self.timeformat)
    self.response_tuple.append(self.working_area)
    return self.response_tuple

  # Methode prüft, ob eine Zeit innerhalb des Objektzeitraums liegt.
  # Parameter:
      # time_between_hours: Zu überprüfende Zeit Stundenagabe
      # time_between_mins: Zu überprüfende Zeit Minutenangabe
  # Rückgabe: BOOL
      # True: Wenn Zeitangabe innerhalb des timespaces liegt.
      # False: Wenn Zeitangabe nicht innerhalb des timespaces liegt.
  def is_between(self, time_between_hours = "", time_between_mins = ""):
    
    string1 = str(time_between_hours) + ":" + str(time_between_mins)

    if self.timespace_string[:5] < self.timespace_string[-5:]:
        if self.timespace_string[:5] <= string1 and self.timespace_string[-5:
                                                   ] >= string1:
            return True
        else:
            return False
    else:
        if string1 >= self.timespace_string[:5] or self.timespace_string[-5:
                                                  ] >= string1:
            return True
        else:
            return False
  pass



