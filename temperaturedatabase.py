#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# ---Datenbank-Klasse Bewässerung----------------------------------------------
# ---Softwareprojekt ET19A Gruppe 13-------------------------------------------
# ---Version 1.2---------------------------------------------------------------
# -----------------------------------------------------------------------------

import os
from datetime import datetime
import time
import io
import mysql.connector
import sys
import random


# -----------------------------------------------------------------------------
# -Klasse, die die Temperaturmesswerte des Sensehat mit dem aktuellen Datum und
# -Uhrzeit in eine Datenbank speichert und diese für 4 Wochen speichert--------
# -----------------------------------------------------------------------------
class temperaturedatabase(object):
    
   

    # Konstruktor
    # Parameter:
        # host: Hostname (string)
        # user: Benutzername (string)
        # password: Passwort (string)
        # database: Name der Datenbank (string)
    # Rückgabe: keine
    def __init__(self, host, user, password, database):
        self.Host = host
        self.User = user
        self.Password = password
        self.Database = database
        self.db_connect() 
        self.border_hour = 0
    
    # Methode stellt die Verbindung mit einer Datenbank her.
    # Parameter: keine
    # Rückgabe: keine
    def db_connect(self):
      try:
         config = {
            'host': self.Host,
            'user': self.User,
            'password': self.Password,
            'database': self.Database
         }
         global mydb, cursor
         mydb = mysql.connector.connect(**config)
         cursor = mydb.cursor()
      
      except:
            print ("Fehler in Ihren angegebenen Parametern!") 
            print ("Überprüfen Sie Ihre eingegebenen Zugangsdaten!")
            sys.exit()

    # Methode unterbricht die Datenbankverbindung.
    # Parameter: keine
    # Rückgabe: keine
    def db_close(self):
      mydb.commit()
      mydb.close()
      print("Datenbankverbindung wurde unterbrochen!")


    # Methode schreibt die aktuelle Uhrzeit/Datum und Temperatur  
    #   alle 60 Minuten in die Datenbank.
    # Parameter: 
        # temp: Temperaturwert (INT)
    # Rückgabe: keine
    def db_insert(self, temp):
      time_right_now = str(datetime.now().strftime(
                "%Y-%m-%d %H:%M:%S"))
      temperature_right_now = str(temp)
      
      # Schützt vor SQL Injection
      if (len(list(c for c in time_right_now if c.isalpha())) == 0) & (
          time_right_now.find("\"") == -1) & (time_right_now.find(";") ==
          -1) & (len(list(c for c in temperature_right_now if c.isalpha()
          )) == 0) & (temperature_right_now.find("\"") == -1) & (
          temperature_right_now.find(";") == -1) & (self.border_hour !=
          int(datetime.now().strftime("%H"))):
              sql = ("INSERT INTO aussentemperatur (Datum, Temperatur)"
              "VALUES('"+time_right_now+"' ,'"+temperature_right_now+"');")
              cursor.execute(sql)
              mydb.commit()
              self.border_hour =  int(datetime.now().strftime("%H"))
      pass

    # Methode überprüft welche Daten älter als 4 Wochen sind.
    # Parameter: keine
    # Rückgabe: keine
    def db_date_delete(self):
      # Ermittlung der Minutenanzahl, ab dem die Daten älter als eine Minute sind
      sql = "DELETE FROM aussentemperatur WHERE DATEDIFF(NOW(), Datum) >=28;"
      cursor.execute(sql)
      mydb.commit()




