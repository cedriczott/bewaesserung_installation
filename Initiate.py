#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# ---Initiate-Klasse Bewässerung-----------------------------------------------
# ---Softwareprojekt ET19A Gruppe 13-------------------------------------------
# ---Version 1.1---------------------------------------------------------------
# -----------------------------------------------------------------------------

#Datenspeicherung der Timespaces im Jason-Format
import json

# -----------------------------------------------------------------------------
# ---Steuert den Import und Export in die Initiate-Datei-----------------------
# -----------------------------------------------------------------------------
class initiate(object):

   # Konstruktor
   # Parameter: 
     # operation_mode: Betriebsmodus (string)
     # timespaces_ini: Liste aller Timespaces (list)
   # Rückgabe: keine
   def __init__(self, operation_mode = "", timespaces_ini = []):
    
     # Initiation-Datei als Objekt. Mit Fehlerbehandlung, 
     # wenn Datei nicht geöffnet.
     try:
        self.Initiate_File = open("Initiation.txt", "r+") 
     except FileNotFoundError:
        self.Initiate_File = open("Initiation.txt", "a") 
        self.Initiate_File = open("Initiation.txt", "r+") 
        
     # Speicher des Betriebsmodus für Initial-Export.
     self.Ini_mode = operation_mode                   

     # Speicher für alle Timespaces in einem String.
     self.Ini_timespace = timespaces_ini   
     
   # Destruktor
   # Parameter: keine
   # Rückgabe: keine
   def __del__(self):

     pass

   # Importiert die Startparameter aus der Ini-Datei.
   # Parameter: keine
   # Rückgabe: 
     # ini_import: Beinhaltet gespeicherten Betriebsmodus (erstes 
     #            Element) und alle Zeitintervalle (restliche 
     #            Listenelemente) (list)
   def import_method(self):
    
     ini_import = []
     ini_import_tuple = []
     ini_import_list = []

     # ------------------------------------------------------------------------
     # ---For-Schleife, die Zeilenweise aus der Initiation-Datei liest---------
     # ------------------------------------------------------------------------
     for ini_import_file in self.Initiate_File:

        ini_mode_import = ini_import_file.find("Betriebsmodus")

        # Prüft, ob eingelesene Zeile "Betriebsmodus" enthält und ob Liste leer
        # ist. Erstes Listenelement ist immer Betriebsmodus.
        if ini_mode_import != -1:           
            store = ini_import_file[15:] 

            # Aufgrund der Formatierung der Textdatei benötigt. 
            # (entfernt '\n' am Ende)
            ini_import.append(store[:-1])                         
            continue

        ini_timespace_import = ini_import_file.find("Zeiträume")

        if ini_timespace_import != -1:
            store = ini_import_file[11:]
            initiate_input_object = json.loads(store)
            ini_import_tuple.append(initiate_input_object["ID"]) 
            ini_import_tuple.append(initiate_input_object["Time"]) 
            ini_import_tuple.append(initiate_input_object["Area"]) 
            ini_import_list.append(ini_import_tuple)
            continue

     self.Initiate_File.close()
     ini_import.append(ini_import_list)
     return ini_import

   # Exportiert die Startparameter aus der Ini-Datei.
   # Parameter: keine
   # Rückgabe: keine
   def export_method(self):

     ini_timespace_string = ""

     # Löschen des bisherigen Inhalts der Datei.
     self.Initiate_File.truncate(0)

     # ------------------------------------------------------------------------
     # ---Schleife, welche die einzelnen Listenelemente zu einem String-------- 
     # ---zusammenfügt---------------------------------------------------------
     # ------------------------------------------------------------------------
     for tuple_timespace in self.Ini_timespace:
        ini_timespace_string += "Zeiträume: "
        #Objekt zur Handhabung der Daten.
        initiate_output_object = {
             "ID": tuple_timespace[0],
             "Time": tuple_timespace[1],
             "Area": tuple_timespace[2] 
         }
        ini_timespace_string += json.dumps(initiate_output_object)
        ini_timespace_string += "\n"

     self.Initiate_File.write("Betriebsmodus: " + self.Ini_mode + "\n" 
                             + ini_timespace_string)
     self.Initiate_File.close()


 